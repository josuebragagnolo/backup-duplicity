#!/bin/bash
## Back up files to MEGA using Duplicity.

SCRIPT=$(readlink -f ${0})         # Absolute path to this script. /home/user/bin/foo.sh
SCRIPTPATH=$(dirname ${SCRIPT})    # Absolute path this script is in. /home/user/bin
SCRIPTNAME=$(basename ${SCRIPT})   # Name of this script. foo.sh

###############################################################################
# Start user config                                                           #
###############################################################################

ENV="${SCRIPTPATH}/.env"
if [ -f ${ENV} ]
then
    source ${ENV}
else
    echo "You must to create ${ENV} based on .env_exemple on same dir"
    exit 1
fi


###############################################################################
# End user config                                                             #
###############################################################################

if [[ ${encrypt} == "true" ]]
then
    export SIGN_PASSPHRASE="$PASSPHRASE"
    key_options="--encrypt-key $encrypt_key --sign-key $sign_key"
else
    key_options="--no-encryption"
fi

target_url="megav3://$mega_username:$mega_password@mega.nz/$mega_dir?no_logout=1"

include_directories=""
for directory in ${backup_directories[@]}; do
    include_directories+="--include $directory "
done

# Perform the backup
duplicity $key_options \
          --full-if-older-than $full_backup_if_last_older_than \
          --asynchronous-upload \
          --exclude ${LOG} \
          $include_directories \
          --exclude "**" \
          "/" "$target_url" \
          2>&1 | tee -a ${LOG}

# Remove old backups
duplicity remove-all-but-n-full $keep_n_full_backups \
          $key_options \
          --asynchronous-upload \
          --force \
          "$target_url" \
          2>&1 | tee -a ${LOG}


#restore
function restore () {
    duplicity restore $key_options "$target_url" $1
}