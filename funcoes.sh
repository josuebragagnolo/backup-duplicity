
SCRIPT=$(readlink -f ${0})         # Absolute path to this script. /home/user/bin/foo.sh
SCRIPTPATH=$(dirname ${SCRIPT})    # Absolute path this script is in. /home/user/bin
SCRIPTNAME=$(basename ${SCRIPT})   # Name of this script. foo.sh


function log(){
	if $DEBUG
	DIR_LOG="${SCRIPTPATH}"
	then
		if [ ! -d $DIR_LOG ]
		then
			mkdir -p $DIR_LOG
		fi
		
		echo "$(date +"%Y-%m-%d %H:%M:%S") - $SCRIPTNAME - $1" | tee -a $DIR_LOG/${SCRIPTNAME}.log
	fi
}

function emExecucao {
	if [[ $(pidof -x $(basename $0) -o %PPID) ]]; 
	then
		echo $(pidof -x $(basename $0) -o %PPID)
		log "Ja possui processo em execucao "
		exit 1
	fi    
}    

function rotate
{
	archive=$1
	#Keep a maximum of $BACKUP_NUM number of backups
	if [ -f $archive.$BACKUP_NUM ]
	then 
		rm -f $archive.$BACKUP_NUM
	fi
	n=$(( $BACKUP_NUM - 1 ))
	while [ $n -gt 0 ]
	do
		if [ -f $archive.$n ]
		then
			mv $archive.$n $archive.$(( $n + 1 ))
		fi
		n=$(( $n - 1 ))
	done
	if [ -f $archive ]
	then 
		mv $archive $archive.1
	fi
}

function testaPwdBkp {
	if [ ! -d $PWD_BKP ]
	then
		mkdir -p $PWD_BKP
	fi
}

function criaDir () {
	diretorio=$1
	if [ ! -d $diretorio ]
	then
		mkdir -p $diretorio
	fi
}
